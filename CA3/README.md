CA3: README
==============

Find detailed links to each CA3 section here:

[Part1](https://bitbucket.org/manel_rga/devops-20-21-1201770/src/master/CA3/Part1/README.md)

[Part2](https://bitbucket.org/manel_rga/devops-20-21-1201770/src/master/CA3/Part2/README.md)

[Alternative](https://bitbucket.org/manel_rga/devops-20-21-1201770/src/master/CA3/HyperV/README.md)