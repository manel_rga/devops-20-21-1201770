# CA4: Part2 README


The Readme is structure in 1 section:

- **Kubernetes Steps** This part of CA4 focuses on using Kubernetes to run the CA3/Part2 project;

## CA4: Kubernetes Steps

#### 1. Install Kubernetes

#### 1. Install Kind
Used chocolatey
```
choco install kind
```

#### 1. Check if kubernetes is up and running:
```
kubectl get nodes
```
#### 1. Change yaml files to include correct images:
```
bla bla bla
```
#### 1. Change yaml files to include correct IPs:
```
bla bla bla
```
#### 1. Deploy application to Kubernetes:
```
kubectl apply -f os ficheiros yaml
```
![img.png](img.png)

#### 1.1. Clone the project supplied in class:
```
git clone https://manel_rga@bitbucket.org/atb/docker-compose-spring-tut-demo.git
```
___
#### 1.2. Check if the project is working as intended:

In the project root directory:
```
docker-compose up
```
Everything seems to be on the up and up:

![img.png](img.png)
___
#### 1.3. Check if application is running:
```
http://localhost:8080/basic-0.0.1-SNAPSHOT/
```
![img_1.png](img_1.png)

and:
```
http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
```
Changing the ```JDBC URL``` to:
```
jdbc:h2:tcp://192.168.33.11:9092/./jpadb
```

![img_2.png](img_2.png)

#### 1.4. It's alive!

### 2. Get your CA2/Part2 gradle project working!

#### 2.1. Check the Dockerfiles
Going through db\Dockerfile shows that it requires no changes, as our project's DB will be using the same settings as the example's.

web\Dockerfile requires some changes, though:

![img_3.png](img_3.png)

As you can see, it clones a repository, moves to a directory in that repository and copies a file.

#### 2.2. Change web\Dockerfile

Since we'll be using our own repository instead of the example provided in the class we need to change these fields to match our own repository and files:

```
RUN git clone https://manel_rga@bitbucket.org/manel_rga/devops-20-21-1201770.git

WORKDIR devops-20-21-1201770/CA3/Part2

RUN ./gradlew clean build

RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
```

#### 2.3. Remove the previously created Containers
In the root folder, remove the containers after stopping the process. We can see the containers' names when they are stopped:

![img_4.png](img_4.png)
```
docker rm part1_web_1
docker rm part1_db_1
```

#### 2.4. Run the build command again:

We need to update the settings used to create the containers, so run the following command:

```
docker-compose build
```
#### 2.5. Build fails because ./gradlew clean build fails
Having this issue 2 or 3 times is apparently not enough to learn this. Give the proper authorizations to gradlew file!
![img_5.png](img_5.png)

In the web\Dockerfile include the command to give the execute authorization to gradlew.

```
RUN chmod u+x gradlew
```
![img_6.png](img_6.png)

#### 2.6. Run the build command again 2: Electric Boogaloo:
```
docker-compose build
```
#### 2.7. Raise the Containers
```
docker-compose up
```
#### 2.8. Access the localhost to check on the DB tables:

![img_8.png](img_8.png)

![img_7.png](img_7.png)

### 3. We're done with the Part1 of CA4!
