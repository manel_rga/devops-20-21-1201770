The Readme is structure in 5 sections:

- **CA1 Steps** : this section works like a step-by-step tutorial of the process followed in this CA(further details on the implementation can be found in the implementation section);
- **Requirements** : this section contains the requests made by the client;
- **Analysis**: a small analysis of the requests introduced in this CA;
- **Design**: details on the design of the solutions;
- **Implementation**: detailed displays of the chosen implementation(methods, classes, etc...);
- **git Alternatives**: a discussion and demo of alternatives to git.

**CA1 Steps**

1.1 Setup project structure and Spring;
___
2.1 Create a new Tag:
```
   git tag v.1.2.0
```
2.2 Verify the creating of the Tag:
```
   git tag
```
___

3.1 Add files staging:

- if adding all files use:
```
   git add .
```
- if adding specific files use:
```
   git add filename
```
3.2 Commit changes
```
   git commit -m "initial commit"
```
___
4.1 Link local and remote repository
```
   git remote add origin https://manel_rga@bitbucket.org/manel_rga/devops-20-21-1201770.git
```
4.2 Push files to remote repository
```
   git remote add upstream https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git
```
___
5.1 Create a branch with "email-field" name:
```
   git branch email-field
```
5.2 Verify the creation of the branch:
```
   git branch
```
5.3 Change branches:
```
   git checkout email-field
```
___
6.1 Add Support for email field in Employee.java;

6.2 Add Support for email field in DatabaseLoader.java;

6.3 Add Support for email field in app.js;
___
7.1 Add unit tests for the creation of Employees and validation of Employees;
___
8.1 Ensure Server and Client parts are working as intended by running Spring and loading http://localhost:8080/;
___
9.1 Add all files and commit changes in branch email-field:
```
git add .
```
```
git commit -m "added email functionality and validation for employee creation and its attributes"
```
9.2 Created and pushed the branch to the remote depository:
```
git push --set-upstream origin email-field
```
or
```
git push -u origin email-field
```
___
10.1 Checkout Master:
```
git checkout master
```
10.2 Merge email-field branch into master:
```
git merge email-field
```
10.3 Create a new Tag:
```
   git tag v.1.3.0
```
10.4 Add, commit and push with tag:
```
git add .
```
```
git commit -m "merged email-field and made a few changes to README"
```
```
git push origin master v1.3.0
```
___
11.1 Create a branch with "fix-invalid-email" name:
```
   git branch fix-invalid-email
```
11.2 Verify the creation of the branch:
```
   git branch
```
11.3 Change branches:
```
   git checkout fix-invalid-email
```
11.4 Created and pushed the branch to the remote depository:
```
git push --set-upstream origin fix-invalid-email
```
___
12.1 Add validations for email field in Employee.java (using regex);
___
13.1 Add unit tests for the validation of emails;
___
14.1 Ensure Server and Client parts are working as intended;
___
15.1 Add and commit changes in branch email-field:
```
git add .
```
```
git commit -m "added proper email validation using regex"
```
15.2 Push changes to fix-invalid-email branch:
```
git push origin fix-invalid-email
```
15.3 Checkout Master:
```
git checkout master
```
15.4 Merge fix-invalid-email branch into master:
```
git merge fix-invalid-email
```
15.5 Create a new Tag:
```
   git tag v.1.3.1
```
15.6 Verify tag:
```
   git tag
```
15.7 Add, commit and push:
```
git add .
```
```
git commit -m "merged branch fix-invalid-email"
```
```
git push origin master v1.3.1
```
___
16.1 Create new tag to signalize completion:
```
   git tag ca1
```
16.2 Verify tag:
```
   git tag
```
16.3 Add, commit and push tag:
```
git add .
```
```
git commit -m "merged branch fix-invalid-email"
```
```
git push origin master ca1
```
___

**Requirements**

>*"You should develop new features in branches named after the feature. For instance, a branch named "email-field" to add a new email field to the application"*

>*"You should also add unit tests for testing the creation of Employees and the validation
of their attributes (for instance, no null/empty values)."*

>*"The server should only accept Employees with a valid email (e.g., an email must have the "@" sign)"*

Based on these notes, the requirements were assumed to be:

- Create email field for Employee.

- Validate and unit test attributes of Employee (including newly created email field).
  - Validation of email field: email must have the "@" sign.

___
**Analysis**

Given the requests, to implement the changed it is required that:

- Email field needs to be added in front and back-end so it is possible to display it.

- Employee attributes must be validated to ensure that only valid values are added.

___
**Design**

For the first request, a new email attribute, of the type String, should be added to the class Employee, in its constructor. This is done in a new, feature specific, branch. This attribute will store the email data of the employee.

Furthermore, employee attributes should be validated - the email field will be validated using Regex, while the other attributes can be just validated to ensure they have no null, empty or blank values.

Finally, the front-end will need to be updated to allow the display of the Employee information. The DatabaseLoader class as well as the app.js file will need to be updated for this purpose.

___
**Implementation**

- The Employee class attributes and Constructor were updated as follows:

```
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private String email;

    public Employee() {
    
    }

    public Employee(String firstName, String lastName, String description, String jobTitle, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.jobTitle = jobTitle;
        this.email = email;
        validateData();
    }

[...]

}
```

The validation of the data is separated into two elements: string Validation and email specific validation.

String Validation - since all attributes are Strings they can all be validated the same for null, empty or blank values.
If an invalid value is inputted an error message is thrown.
```
[...]
private void validateData() {
        this.firstName = validateString(this.firstName);
        this.lastName = validateString(this.lastName);
        this.description = validateString(this.description);
        this.jobTitle = validateString(this.jobTitle);
        checkEmailValidity();
    }

    private String validateString(String string) {
        if (string == null)
            throw new IllegalArgumentException ("Input is invalid or there hasn't been an input in field.");
        else if (string.isEmpty() || string.trim().length() == 0)
            throw new IllegalArgumentException ("Input is invalid or there hasn't been an input in field.");
        return string;
    }

```

___
**git Alternatives**

The chosen alternative was Plastic SCM.

Plastic SCM is a version control system that allows for both centralized or distributed usage.

The reasons why it was chosen was it's emphasis on a GUI and the possibility of using a centralized system, creating a nice contrast to the GIT usage.

Plastic SCM is catered to both developers and artists, making it particularly interesting for fields like videogames, especially as it allows for easier management of very big files.
It works in similar fashion to GIT, allowing for easy branching and merging as well as pushing and pulling, but doesn't allow rebasing at all, for now.

A few of the common terms in Plastic SCM are:

- checkin - same as commiting in GIT;
- main - same master branch in GIT;
- changeset - same as a commit inn GIT;
- update - same as checkout in GIT;
- workspace - same as the working copy in GIT;

___
**Plastic SCM Steps**

**For the purposes of this assignment, the server where the centralized repository was stored was my own laptop, which makes it impossible to actually check the project. If required, a demo can be shown live.**

1.1 Setup and install Plastic SCM;

1.2 Plastic for developers option was chosen - Plastic SCM provides a simpler GUI for artists and a more comprehensive GUI for developers;

1.3 The option for a centralized mode was chosen - to provide a different experience than GIT;

1.4 A repository was created with name "CA1" with server devops_21_21_1201770@cloud;

![img_4.png](tut-basic/images/img_4.png)

![img.png](tut-basic/images/img.png)
1.5 A workspace was created with name "CA1";

![img_1.png](tut-basic/images/img_1.png)
1.6 The tut-basic folder was copied to the folder created in the designated path;
___

2.1 To convert the currently private objects(files) to source-controlled items:
 
  ![img_2.png](tut-basic/images/img_2.png)
- Move to the "Workspace Explorer" area;
- Right-click the root workspace folder - Select "Add directory tree to source control", this is similar to the "git add " command;
 
  ![img_3.png](tut-basic/images/img_3.png)
- Right-click the root workspace folder - Select "Checkin" and with all objects selected click the "Checkin" button, this is similar to the "git commit" command;
  
![img_5.png](tut-basic/images/img_5.png)
___
3.1 Create a new Label:

- Head to Branch Explorer;

![img_6.png](tut-basic/images/img_6.png)

- Right-click the initial checkin we've just made and select "Label this changeset...";

![img_7.png](tut-basic/images/img_7.png)

- Fill in the name Name and Comments sections;

![img_8.png](tut-basic/images/img_8.png)

- The Branch Explorer view now looks like this:

![img_9.png](tut-basic/images/img_9.png)
___
4.1 Creating the new branch for email feature:

- Right-click current changeset and select "Create branch from this changeset...":

![img_10.png](tut-basic/images/img_10.png)

- Input the name and any comments for the new branch:

![img_11.png](tut-basic/images/img_11.png)

- The Branch Explorer view now shows the new branch:

![img_12.png](tut-basic/images/img_12.png)

4.2 Make changes to implementation;

4.3 In the Workspace Explorer view right-click the new Test folder and select "Add to source control":

![img_24.png](tut-basic/images/img_24.png)

4.4 Checkin the changes in the new branch:

- In the Pending Changes view review the differences between the previous file state and the current, if needed, by clicking "Show diff":

![img_13.png](tut-basic/images/img_13.png)
![img_14.png](tut-basic/images/img_14.png)

- Checkin the changes by clicking "Checkin":

![img_15.png](tut-basic/images/img_15.png)
___
5.1 Switch to main changeset (currently v1.2.0):

![img_16.png](tut-basic/images/img_16.png)

5.2 Merge branch into main changeset (currently v1.2.0) by rich-clicking the email-field branch and selecting "Merge from this changeset"

![img_17.png](tut-basic/images/img_17.png)

- Click "Apply Changes":

![img_18.png](tut-basic/images/img_18.png)

- Checkin the changes to the changeset by clicking "Checkin" in the "Pending changes" view:

![img_19.png](tut-basic/images/img_19.png)

5.3 Add label to main:

- In the "Branch explorer" view, right-click the most recent changeset and select "Label this changeset":

![img_21.png](tut-basic/images/img_21.png)
![img_20.png](tut-basic/images/img_20.png)
___
6.1 Create new branch called "fix-invalid-email" by right-clicking the v1.3.0 changeset and selecting "Create branch from this changeset":

![img_22.png](tut-basic/images/img_22.png)

6.2 Input branch name and appropriate comments;

![img_23.png](tut-basic/images/img_23.png)
___
7.1 Switch to main changeset (currently v1.3.0):

![img_25.png](tut-basic/images/img_25.png)

7.2 Merge branch into main changeset (currently v1.3.0) by rich-clicking the fix-invalid-email branch and selecting "Merge from this changeset"

![img_26.png](tut-basic/images/img_26.png)

- Click "Apply Changes":

![img_18.png](tut-basic/images/img_18.png)

- Checkin the changes to the changeset by clicking "Checkin" in the "Pending changes" view:

![img_19.png](tut-basic/images/img_19.png)

7.3 Add label to main:

- In the "Branch explorer" view, right-click the most recent changeset and select "Label this changeset":

![img_27.png](tut-basic/images/img_27.png)
![img_28.png](tut-basic/images/img_28.png)

8.1 Add README info and label "ca1" signifying end of assignement:
- In the "Pending changes" view, click the "Checkin" button;

- In the "Branch explorer" view, right-click the most recent changeset and select "Label this changeset":

![img_29.png](tut-basic/images/img_29.png)
![img_30.png](tut-basic/images/img_30.png)

___